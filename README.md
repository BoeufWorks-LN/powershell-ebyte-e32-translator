# PowerShell Ebyte E32 Translator

Simple decoder for Ebyte E32 LoRa modules.

Based on https://github.com/sandeepmistry/arduino-LoRa/issues/203#issuecomment-770967581

## Working procedure

1. Enter encoded data content into "input" variable
2. Enter the first character of the encoded message
3. Let the magic happen ...

## License

__GNU GPLv3__
