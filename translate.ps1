### PowerShell Ebyte LoRa Decoder
#   23/08/21 PowerShell 5
### Git Repo -> https://gitlab.com/BoeufworksLN/powershell-ebyte-e32-translator

$input = "0x55, 0x66, 0x69, 0x56, 0x55, 0x55, 0x55, 0x55, 0x9a, 0x56, 0x96, 0xa5, 0x96, 0x66, 0x96, 0x66, 0x96, 0x69, 0x95, 0xa6, 0x57, 0x89"
$firstChar = "H"



### INPUT TRANSLATION

# Convert input to an array for easier processing
$input = $input.toLower() -split ", "

# Dictionary for translation
$dictionary = "0x55","0x56","0x59","0x5a","0x65","0x66","0x69","0x6a","0x95","0x96","0x99","0x9a","0xa5","0xa6","0xa9","0xaa"

# Create temp array to store translatedInput
$translatedInput = ""

for ($i = 0; $i -lt $input.Count-2; $i++) {
    # Prepend 0x to the transated value if index is impair
    if(($i % 2 -eq 0)) {$translatedInput += "0x"}

    # Get translation from dictionary
    $translatedInput += ('{0:x}' -f $dictionary.indexOf($input[$i]))

    # Append ", " to the value if index is pair, excludes the last empty item (-3)
    if(($i % 2 -gt 0) -and ($i -lt $input.Count - 3)) {$translatedInput += ", "} 
}

# Append the CRC to the end of the translation, it does not need translation
$translatedInput += ", " + $input[$input.Count - 2] + ", " + $input[$input.Count - 1]

# Convert translatedInput to an array for easier processing
$translatedInput = $translatedInput -Split ", "



### LENGTH EXTRACTION

# Number of bytes for payload is bytes 0 and 1, up to 58
$nbBytes = [int32]$translatedInput[0]



### KEY INDEX EXTRACTION

# Index of the decryption key is bytes 2 and 3
$keyIndex = $translatedInput[1]



### ADDRESS EXTRACTION

# Address for the packet is bytes 4 to 7 (MSB is 4 LSB is 7)
$address = $translatedInput[2] + $translatedInput[3].subString(2)



### KEY CALCULATION

# Key for decoding the data packet
$Key = [int32]$firstChar[0] -bxor [int32]($translatedInput[4])



### DATA EXTRACTION

# Data is bytes 8 to n-5
# Storing multiple values as they could be used later
# Using collections as it is cleaner than strings for adding data
$encodedData        = New-Object System.Collections.Generic.List[int]
$decodedData        = New-Object System.Collections.Generic.List[int]
$decodedDataAscii   = New-Object System.Collections.Generic.List[char]
for ($i = 0; $i -lt $nbBytes; $i++) {
    $encodedData.add($translatedInput[4 + $i])
    $decodedData.add($encodedData[$i] -bxor $key)
    $decodedDataAscii.add($decodedData[$i])
}



### CHECKSUM EXTRACTION

# Checksum for the packet is bytes n-4 and n-5
$CheckSum = $translatedInput[$translatedInput.Length - 3]



### CHECKSUM CALCULATION

# Init var for storing checksum
$calculatedChecksum = 0
# Adding everything into $calculatedChecksum
for ($i = 0; $i -lt ($translatedInput.Count - 3); $i++) {
    $calculatedChecksum += [int32]$translatedInput[$i]
}

# Invert bitwise and complement
$calculatedChecksum = -bnot ($calculatedChecksum % 256) + 1

# Fix for powershell bnot on 32bits (32-8=24)
$calculatedChecksum = ([convert]::ToString($calculatedChecksum,2)).subString(24)

# Formatting checksum for compliance
$calculatedChecksum = "0x"+("{0:X}" -f [convert]::ToInt32($calculatedChecksum,2)).toLower()



### OUTPUT
Write-Host ("Input: `t`t") -NoNewLine
Write-Host ($input + "`n") -Separator ", " -ForegroundColor black -BackgroundColor White

Write-Host ("Translation: `t") -NoNewLine
Write-Host ($translatedInput + "`n") -Separator ", " -ForegroundColor black -BackgroundColor White

Write-Host ("Data Size: `t") -NoNewLine
Write-Host ([string]$nbBytes + " Bytes" + "`n") -ForegroundColor black -BackgroundColor White

Write-Host ("LoRa Address: `t") -NoNewLine
Write-Host ("{0:d4}" -f $address + "`n") -ForegroundColor black -BackgroundColor White

Write-Host ("Key Index: `t") -NoNewLine
Write-Host ($keyIndex + "`n") -ForegroundColor black -BackgroundColor White

if ($CheckSum -eq $calculatedChecksum) {
    $color = "Green"
    $validity = "True"
}
else {
    $color = "Red"
    $validity = "False"
}
Write-Host ("CheckSum: `t") -NoNewLine
Write-Host ($CheckSum) -ForegroundColor black -BackgroundColor $color
Write-Host ("Validity: `t") -NoNewLine
Write-Host ($validity) -ForegroundColor black -BackgroundColor $color
Write-Host ("") # Bugfix

$dataOut = ""
Write-Host ("Raw Data: `t") -NoNewLine
$encodedData | ForEach-Object { $dataOut += "0x{0:X}" -f $_ + ", "}
$dataOut = $dataOut -replace "..$"
Write-Host ($dataOut + "`n") -Separator ", "  -ForegroundColor black -BackgroundColor White
$dataOut = ""

Write-Host ("Decoded Data: `t") -NoNewLine
$decodedData | ForEach-Object { $dataOut += "0x{0:X}" -f $_ + ", "}
$dataOut = $dataOut -replace "..$"
Write-Host ($dataOut + "`n") -Separator ", "  -ForegroundColor black -BackgroundColor White

Write-Host ("Decoded Data (ascii): `t") -NoNewLine
Write-Host ( $decodedDataAscii + "`n") -Separator "" -ForegroundColor black -BackgroundColor White

write-output ("Key: " + $key)
write-output ($input[1])
